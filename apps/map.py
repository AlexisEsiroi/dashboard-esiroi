# region Importation des librairie
from dash import html
import plotly.express as px
import pandas as pd
from dash import dcc
import dash_bootstrap_components as dbc
from lib import librairieGeolocalisation as lb
import pathlib
import datetime
from dash.dependencies import Input,Output,State
# endregion

# region Lecture des données CSV dans le dossier spécial
PATH = pathlib.Path(__file__).parent
DATA_PATH = PATH.joinpath("../datasets").resolve()
dataFrameESIROI = pd.read_csv(DATA_PATH.joinpath("dataESIROI.csv"))
dataFrameGateway = pd.read_csv(DATA_PATH.joinpath("gateways_info_summary.csv"))
# endregion

localisation = lb.getAllLocation(dataFrameESIROI)
gateways = lb.getAllGateway(dataFrameESIROI, localisation)
capteurs = lb.getAllDevice(dataFrameESIROI,gateways)
print(vars(gateways[0]))
print(capteurs)
donneesCapteurLocalisation = lb.locationData(dataFrameGateway)
lb.tauxDePerteAllGateway(donneesCapteurLocalisation, gateways, dataFrameESIROI)
mapCapteur = px.scatter_mapbox(donneesCapteurLocalisation, lon="lng", lat="lat", zoom=4, height=600,width=1300, mapbox_style="open-street-map", color="network",hover_data=["name","TauxDePerte"])  # ["name", "TauxDePerte"]
nbCapteurlocalisation = lb.nbDeCapteursParLocalisation(dataFrameESIROI,lb.getAllLocation(dataFrameESIROI))
camenbertLocalisation = px.pie(nbCapteurlocalisation,values="capteurs",names="localisations")

layout = html.Div([
    dcc.Graph(
        id="graphGeolocalisation",
        figure=mapCapteur,
    ),
       dbc.Row([
        dbc.Col(
            html.H1([dbc.Badge("Nombre de tortues par zone", color="info", className="ms-1")]),
            width=4,
        ),
         dbc.Col(
            html.H1([dbc.Badge("Répartition des tortues par gateway", color="info", className="ms-1")]),
            width=7
        ),
    ]),
    dbc.Row([
        dbc.Col(width=4),
        dbc.Col(
            dbc.Select(
                id="localisationCamenber",
                placeholder="Choix du capteur",
                options=[
                    {'label' : capteur , 'value' : capteur} for capteur in capteurs
                ],
            ),
            width=3
        ),
        dbc.Col(
            dcc.DatePickerRange(
                id='rangeLocalisationCapteur',
                min_date_allowed=datetime.date(2021, 1, 1),
                max_date_allowed=datetime.datetime.now(),
                initial_visible_month=datetime.date(2021, 6, 24),
                end_date=datetime.datetime.now()
            ),
            width=3
        ),
        dbc.Col(
            dbc.Button(
                children='Générer',
                id="genererGraphiqueLocalisationCapteur",
                n_clicks=0,
                outline=True,
                color="success",
                className="me-1"
            ),
            width=1
        ),
    ]),
    dbc.Row([]),
    dbc.Row([
        dbc.Col(
            dcc.Graph(
                id="graphNbCapteur",
                figure = camenbertLocalisation,
            ),
            width=4
        ),
        dbc.Col(
              dcc.Graph(
                id='graphNbCapteurGateway',
                figure={},
            ),
            width=7
        )
    ]),
])
