# region Importation des librairie
from this import d
from typing_extensions import OrderedDict
from dash import html
import plotly.express as px
import pandas as pd
from dash import dcc
import dash_bootstrap_components as dbc
from lib import librairieGeolocalisation as lb
import pathlib
from dash import dash_table as dt
from dash.dependencies import Input, Output, State
import datetime
# endregion

# region Lecture des données CSV dans le dossier spécial
PATH = pathlib.Path(__file__).parent
DATA_PATH = PATH.joinpath("../datasets").resolve()
dataFrameESIROI = pd.read_csv(DATA_PATH.joinpath("dataESIROI.csv"))
dataFrameGateway = pd.read_csv(DATA_PATH.joinpath("gateways_info_summary.csv"))
# endregion

localisation = lb.getAllLocation(dataFrameESIROI)
gateways = lb.getAllGateway(dataFrameESIROI, localisation)
devices = lb.getAllDevice(dataFrameESIROI, gateways)
dateDebut = lb.convertStringToTimeStamp("2021-06-24T00:00:00Z")
dateFin = lb.convertStringToTimeStamp("2021-06-26T00:00:00Z")
print("Date de début de la période : ", dateDebut)
print("Date de fin de la période : ", dateFin)
df = pd.DataFrame(lb.genererDataFrameTab(dataFrameESIROI, dateDebut, dateFin, gateways))
dfNbMess = pd.DataFrame(lb.tabNbMessCapteurGateway(dataFrameESIROI, dateDebut, dateFin, gateways, devices))

layout = html.Div([
    # region Générateur de graphique
    html.H1([dbc.Badge("Générateur de graphiques", color="info", className="ms-1")]),
    dbc.Row([
        dbc.Col(
            dbc.Select(
                id="ChoixX",
                placeholder="Choix de Y",
                options=[
                    {'label': 'RSSI', 'value': 'rssi'},
                    {'label': 'SNR', 'value': 'loRaSNR'},
                ],
            ),
        ),
        dbc.Col(
            dbc.Select(
                id="ChoixLocalisation",
                placeholder="Localisation",
                options=[
                    {'label': ile, 'value': ile} for ile in localisation
                ],
            ),
        ),
        dbc.Col(
            dbc.Select(
                id="ChoixGateway",
                placeholder="Gateway",
                options=[],
            ),
        ),
        dbc.Col(
            dbc.Select(
                id="ChoixDevice",
                placeholder="Device",
                options=[],
            ),
        ),
        dbc.Col(
            dbc.Select(
                id="choixTypeGraphique",
                placeholder="Graphique",
                options=[
                    {'label': "Point", "value": "point"},
                    {'label': "Ligne", "value": "ligne"},
                    {'label': "Ligne avec point", "value": "lignePoint"},
                    {'label': "Histogramme", "value": "histogramme"},
                ],
            ),
        ),
        dbc.Col(
            dcc.DatePickerRange(
                id='my-date-picker-range',
                min_date_allowed=datetime.date(2021, 1, 1),
                max_date_allowed=datetime.datetime.now(),
                initial_visible_month=datetime.date(2021, 6, 24),
                end_date=datetime.datetime.now()
            ),
        ),
        dbc.Col(
            dbc.Button(
                children='Générer',
                id="generationGraphique",
                n_clicks=0,
                outline=True,
                color="success",
                className="me-1"
            ),
        ),
    ]),
    dcc.Graph(
        id="graphiqueSNRRSSI",
        figure={},
    ),
    # endregion


    # region Tableau de synthèse
    html.H1([dbc.Badge("Tableau de synthèse", color="info", className="ms-1")]),
    dcc.DatePickerRange(
        id='DateTableau',
        min_date_allowed=datetime.date(2021, 1, 1),
        max_date_allowed=datetime.datetime.now(),
        initial_visible_month=datetime.date(2021, 6, 24),
        end_date=datetime.datetime.now()
    ),
    dbc.Button(
        children='Update',
        id="updateTableau",
        n_clicks=0,
        outline=True,
        color="success",
        className="me-1"
    ),
    dt.DataTable(
        id='table',
        columns=[{"name": i, "id": i} for i in df.columns],
        data=df.to_dict('records'),
    ),
    # endregion


    # region Graphique nombre de message envoyé
    html.H1([dbc.Badge("Graphique du nombre de message envoyés", color="info", className="ms-1")]),
    dbc.Row([
        dbc.Col(
            dbc.Select(
                id="ChoixMessagesGateway",
                placeholder="Gateway",
                options=[
                    {'label': gateway.gatewayName, 'value': gateway.gatewayName} for gateway in gateways
                ],
            ),
        ),
        dbc.Col(
            dbc.Select(
                id="ChoixMessagesDevice",
                placeholder="Device",
                options=[
                    {'label': device, 'value': device} for device in devices
                ],
            ),
        ),
        dbc.Col(
            dcc.DatePickerRange(
                id='Time',
                min_date_allowed=datetime.date(2021, 1, 1),
                max_date_allowed=datetime.datetime.now(),
                initial_visible_month=datetime.date(2021, 6, 24),
                end_date=datetime.datetime.now()
            ),
        ),
        dbc.Col(
            dbc.Button(
                children='générer',
                id="graphiqueMessagesEnvoyes",
                n_clicks=0,
                outline=True,
                color="success",
                className="me-1"
            ),
        ),
    ]),
    dcc.Graph(
        id="GraphiqueNombreDeMessage",
        figure={},
    ),
    # endregion

    # region Tab nombre de message envoyés
    html.H1([dbc.Badge("Tableau nb mess envoyés", color="info", className="ms-1")]),
    dt.DataTable(
        id='tableNbMess',
        columns=[{"name": i, "id": i} for i in df.columns],
        data=dfNbMess.to_dict('records'),
    )
    # endregion
])
