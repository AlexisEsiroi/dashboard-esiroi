from os import closerange
import pandas as pd
import json
from datetime import datetime as dt
import datetime
import plotly.express as px

#Définir un capteur qui est composé de deux éléments (un nom et un identifiant unique)
class Capteur:
    def __init__(self,deviceName,devEUI) -> None:
        self.deviceName = deviceName
        self.devEUI = devEUI
#Définir d'une gateway qui est composé de deux éléments (un nom et un identifiant unique)
class Gateway:
    def __init__(self,gatewayName,gatewayID) -> None:
        self.gatewayName = gatewayName
        self.gatewayID = gatewayID
    

# fonction qui permet de traiter un fichier CSV
# Et de renvoyer un dataframe
def extractDataFile(filename):
    return pd.read_csv(filename)

#Fonction qui permet de traiter les données géographique
#Récupération de toute les données
def locationData(filename):
    dataLocation = filename.drop(columns = ["shortid"])
    return dataLocation


#Convertion d'une entrée JSON en une sortie de type objet python
def convertJSONtoOBJ(jsonData):
    return json.loads(jsonData)

#Fonction de traitement pour le nom
#Permet de récupérer la localisation à partir du nom de la gateway
def traitementNom(chaine):
    for i in range(1,len(chaine)):
        if(chaine[i].isupper()):
            substring = chaine[:i]
            return(substring)

# Fonction qui permet de retourner toutes les localisations présentent dans le data set
def getAllLocation(data):
    localisation = []
    for capteur in data["Meta"]:
        obj = convertJSONtoOBJ(capteur)
        if(obj["rxInfo"]):
            for gateway in obj["rxInfo"]:
                if(gateway["name"]!=""):
                    gatewayName = traitementNom(gateway["name"])
                    if(gatewayName not in localisation):
                        localisation.append(gatewayName)
    return localisation

#Permet de déterminer toutes les passerelles en fonction de la localisation
def getAllGateway(data,localisations):
    gateways = []
    name = []
    for localisation in localisations:
        for capteur in data["Meta"]:
            obj = convertJSONtoOBJ(capteur)
            if(obj["rxInfo"]):
                for gateway in obj["rxInfo"]:
                    passerelle = Gateway("","")
                    if(gateway["name"]!=""):
                        gatewayName = traitementNom(gateway["name"])
                        if(gatewayName == localisation):
                            if(gateway["name"] not in name):
                                name.append(gateway["name"])
                                passerelle.gatewayName = gateway["name"]
                                passerelle.gatewayID = gateway["gatewayID"]
                                gateways.append(passerelle)
    return gateways

def getAllDevice(data,gateways):
    devices = []
    if(type(gateways[0]) is not Gateway):
        for gateway in gateways:
            for capteur in data["Meta"]:
                obj = convertJSONtoOBJ(capteur)
                if(obj["rxInfo"]):
                    for passerelle in obj["rxInfo"]:
                        if(passerelle["name"]!=""):
                            if(passerelle["name"] == gateway):
                                if(obj["deviceName"] not in devices):
                                    devices.append(obj["deviceName"])
    else:
        for gateway in gateways:
            for capteur in data["Meta"]:
                obj = convertJSONtoOBJ(capteur)
                if(obj["rxInfo"]):
                    for passerelle in obj["rxInfo"]:
                        if(passerelle["name"]!=""):
                            if(passerelle["name"] == gateway.gatewayName):
                                if(obj["deviceName"] not in devices):
                                    devices.append(obj["deviceName"])
    return devices





#Permet de calculer le RSSI (en valeur négative de 0 à -120)
def calculRSSI(valeur):
    return (valeur-255)

#Cette fonction permet de récupérer le RSSI, localisation, le nom du device, le time stamp,nom de la gateway
#Return un objet python
def get_snr(donneesCapteurs):
    snr = []
    for donnee in donneesCapteurs:
        device = Capteur("","")
        donneeObj = convertJSONtoOBJ(donnee)
        device.devEUI = donneeObj["devEUI"]
        device.deviceName = donneeObj["deviceName"]
        if(donneeObj["rxInfo"]):
            for gateways in donneeObj["rxInfo"]:
                gateway = Gateway("","")
                if(gateways["name"]!=""):
                    gateway.gatewayID = gateways["gatewayID"]
                    gateway.gatewayName = gateways["name"]
                    snr.append({
                        "device" : device,
                        "gateway" : gateway,
                        "loRaSNR" : gateways["loRaSNR"],
                        "time" : gateways["time"],
                        "localisation" : traitementNom(gateways["name"]),
                        "fPort" : donneeObj["fPort"],
                    })
    return snr

#Cette fonction permet de récupérer le SNR, localisation, le nom du device, le time stamp,nom de la gateway
#Return un objet python
def get_rssi(donneesCapteurs):
    rssi = []
    for donnee in donneesCapteurs:
        device = Capteur("","")
        donneeObj = convertJSONtoOBJ(donnee)
        device.devEUI = donneeObj["devEUI"]
        device.deviceName = donneeObj["deviceName"]
        if(donneeObj["rxInfo"]):
            for gateways in donneeObj["rxInfo"]:
                gateway = Gateway("","")
                if(gateways["name"]!=""):
                    gateway.gatewayID = gateways["gatewayID"]
                    gateway.gatewayName = gateways["name"]
                    rssi.append({
                        "device" : device,
                        "gateway" : gateway,
                        "rssi" : calculRSSI(gateways["rssi"]),
                        "time" : gateways["time"],
                        "localisation" : traitementNom(gateways["name"]),
                        "fPort" : donneeObj["fPort"],
                    })
    return rssi

#Cette fonction permet de trier un tableau en fonction des valeurs de 
#la localisation ['Europa', 'thesepg', 'Mayotte', 'Aldabra']
def get_localisation(tabData,localisation):
    tabLocalisation = []
    for donnees in tabData:
        if(donnees["localisation"]==localisation):
            tabLocalisation.append(donnees)
    return tabLocalisation

#Fonction qui permet de trier par passerelle
def get_gateway(tabData,gatewayName):
    gatewayNames = []
    for device in tabData:
        if(device["gateway"].gatewayName==gatewayName):
            gatewayNames.append(device)
    return gatewayNames

#Fonction qui permet de trier en fonction du nom d'un device ( capteur positionné sur le dos des tortues)
def get_deviceName(tabData,name):
    deviceNames = []
    for device in tabData:
        if(device["device"].deviceName==name):
            deviceNames.append(device)
    return deviceNames

#Fonction de traitement qui va convertir une date au format time stamp
def convertStringToTimeStamp(dateString):
    return dt.strptime(dateString,'%Y-%m-%dT%H:%M:%SZ')

#Fonction qui permet de convertir une string de type annee-mois-jour en timestamp
def convertDateStringToTimeStamp(dateString):
    return dt.strptime(dateString,'%Y-%m-%d')

#Fonction de convertion de toutes les dates en time stamp
def convertAllDate(tabData):
    for device in tabData:
        dateTimeStamp = convertStringToTimeStamp(device["time"])
        device["time"] = dateTimeStamp

#Fonction qui va trier les données par rapport a une date de début
#et une date de fin
def get_timeStamp(tabData,dateDebut,dateFin):
    convertAllDate(tabData)
    deviceTimeStamp = []
    for device in tabData:
        if(device["time"]>=dateDebut and device["time"]<=dateFin):
            deviceTimeStamp.append(device)
    return deviceTimeStamp

# Fonction qui va permettre de trier le tableau en fonction 
# d'un Fport spécifique
def fPort(tabData,fport):
    tabFport = []
    for device in tabData:
        if(device["fPort"] == fport):
            tabFport.append(device)
    return tabFport

#Adaptation des données et génération du dataFrame
def createDataFrame(donnees,x):
    newTab = []
    if(x=="rssi"):
        for i in range(len(donnees)):
            newTab.append({
                "device" : donnees[i]["device"].deviceName,
                "gateway" : donnees[i]["gateway"].gatewayName,
                "rssi" : donnees[i]["rssi"],
                "time" : donnees[i]["time"],
                "localisation" : donnees[i]["localisation"],
                "fPort" : donnees[i]["fPort"],
            })
    else:
        for i in range(len(donnees)):
            newTab.append({
                "device" : donnees[i]["device"].deviceName,
                "gateway" : donnees[i]["gateway"].gatewayName,
                "loRaSNR" : donnees[i]["loRaSNR"],
                "time" : donnees[i]["time"],
                "localisation" : donnees[i]["localisation"],
                "fPort" : donnees[i]["fPort"],
            })
    dataframe = pd.DataFrame(newTab)
    return dataframe




#Fonction qui permet de générer un graphique automatiquement
def genererGraphique(choixX,location,gateway,deviceName,typeGraphique,dateDeDebut,dateDeFin,data):
    if(choixX=="rssi"):
        tabData = get_rssi(data["Meta"])
    else:
        tabData = get_snr(data["Meta"])
    tabLocalisation = get_localisation(tabData,location)
    tabGateway = get_gateway(tabLocalisation,gateway)
    tabDevice = get_deviceName(tabGateway,deviceName)
    tabTime = get_timeStamp(tabDevice,dateDeDebut,dateDeFin)
    #affichageTableau(tabDevice,"time")
    dataframe = createDataFrame(tabTime,choixX)
    if(typeGraphique=="point"):
        figure = px.scatter(dataframe,x="time",y=choixX)
    elif (typeGraphique == "lignePoint"):
        if(choixX=="rssi"):
            figure = px.line(x=dataframe["time"],y=dataframe["rssi"],labels={'x' : 'TimeStamp', 'y' : "rssi"},markers=True)
        else:
            figure = px.line(x=dataframe["time"],y=dataframe["loRaSNR"],labels={'x' : 'TimeStamp', 'y' : "snr"}, markers=True)
    elif (typeGraphique == "histogramme"):
        figure = px.histogram(dataframe,x="time")
    else:
        if(choixX=="rssi"):
            figure = px.line(x=dataframe["time"],y=dataframe["rssi"],labels={'x' : 'TimeStamp', 'y' : "rssi"})
        else:
            figure = px.line(x=dataframe["time"],y=dataframe["loRaSNR"],labels={'x' : 'TimeStamp', 'y' : "snr"})
    return figure

#Tri device gateway Fcount
def triTauxDePerte(tabData,dateDebut,dateFin):
    triTDP = []
    for capteur in tabData:
        device = Capteur("","")
        donneeObj = convertJSONtoOBJ(capteur)
        device.devEUI = donneeObj["devEUI"]
        device.deviceName = donneeObj["deviceName"]
        for passerelle in donneeObj["rxInfo"]:
            date = convertStringToTimeStamp(passerelle["time"])
            if(date>dateDebut and date<dateFin):
                gateway = Gateway("","")
                gateway.gatewayID = passerelle["gatewayID"]
                gateway.gatewayName = passerelle["name"]
                triTDP.append({
                    "device" : device,
                    "fCnt" : donneeObj["fCnt"],
                    "gateway" : gateway,
                    "time" : passerelle["time"],
                })
    return triTDP

#IOT-014 / EuropaLogon
def triDeviceGateway(deviceName,gatewayName,tab):
    tabDevicegateway = []
    for objet in tab:
        if(objet["device"].deviceName == deviceName and objet["gateway"].gatewayName==gatewayName):
            tabDevicegateway.append(objet)
    return tabDevicegateway

#Calcul du taux de perte 
def tauxDePerteDevice(tabData):
    erreur= 0
    paquetTotalTransmis = 0
    for i in range(0,len(tabData)-1):
        diff= tabData[i+1]["fCnt"] - tabData[i]["fCnt"]
        if(diff!=1):
            erreur = erreur + (diff-1)
    paquetTotalTransmis = tabData[len(tabData)-1]["fCnt"] - tabData[0]["fCnt"]
    return erreur,paquetTotalTransmis

def tauxDePerteGlobal(gateway,tabData):
    devices = []
    erreurTotal = 0
    paquetTransmisTotal = 0
    for obj in tabData:
        if(obj["gateway"].gatewayName==gateway):
            if(obj["device"].deviceName not in devices):
                devices.append(obj["device"].deviceName)
    for device in devices:
        tabDevice = triDeviceGateway(device,gateway,tabData)
        erreur , paquetTransmis = tauxDePerteDevice(tabDevice)
        erreurTotal = erreurTotal + erreur
        paquetTransmisTotal = paquetTransmisTotal + paquetTransmis
    if(paquetTransmisTotal!=0):
        return round(erreurTotal*100/paquetTransmisTotal,2)
    else:
        return 0.0

def tauxDePerteAllGateway(geolocalisation,allgateway,dataESIROI):
    dateDebut = convertStringToTimeStamp("2021-06-24T00:00:00Z")
    dateFin = convertStringToTimeStamp("2021-08-29T00:00:00Z")
    tableau = triTauxDePerte(dataESIROI["Meta"],dateDebut,dateFin)
    tauxDePerte = []
    for i in range(len(geolocalisation["eui"])):
        for gateway in allgateway:
            if(geolocalisation["eui"][i] == gateway.gatewayID and gateway.gatewayName!="MayotteSec-test"):
                tp = tauxDePerteGlobal(gateway.gatewayName,tableau)
                tauxDePerte.append(tp)
    geolocalisation["TauxDePerte"] = tauxDePerte

def nbDeCapteursParLocalisation(data,localisations):
    result = {
        localisations[0] : [],
        localisations[1] : [],
        localisations[2] : [],
        localisations[3] : [],
    }
    for capteur in data["Meta"]:
        dataJSON = convertJSONtoOBJ(capteur)
        for otherGateway in dataJSON["rxInfo"]:
            if(otherGateway["name"]!=""):
                nom = traitementNom(otherGateway["name"])
                if(dataJSON["deviceName"] not in result[nom]):
                    result[nom].append(dataJSON["deviceName"])
    #Traitement du tableau
    newRes = {
        "capteurs" : [],
        "localisations" : []
    }
    for localisation in result:
        for device in result[localisation]:
            newRes["capteurs"].append(1)
            newRes["localisations"].append(localisation)
    return newRes

def nbDeCapteursParGateway(data,nomCapteur,gateways,dateDebut,dateFin):
    result = {
        gateway.gatewayName : [] for gateway in  gateways
    }
    for device in data["Meta"]:
        dataJSON = convertJSONtoOBJ(device)
        if(dataJSON["deviceName"]== nomCapteur[0]):
            for otherGateway in dataJSON["rxInfo"]:
                if(otherGateway["name"]!=""):
                    if(convertStringToTimeStamp(otherGateway["time"])>dateDebut and convertStringToTimeStamp(otherGateway["time"])<dateFin):
                        print(dataJSON["deviceName"])
                        result[otherGateway["name"]].append(nomCapteur[0])
    #Traitement du tableau
    newRes = {
        "capteurs" : [],
        "gateways" : []
    }
    for gateway in gateways:
        compteur = 0
        for device in result[gateway.gatewayName]:
            compteur+=1
        if(compteur!=0):
            newRes["capteurs"].append(compteur)
            newRes["gateways"].append(gateway.gatewayName)
    return newRes

def genererDataFrameTab(dfESIROI,dateDebut,dateFin,gateways):
    tabInfo = {
        gateway.gatewayName : {
            "messMoy" : 0,
            "messRecu" : 0,
            "dateLastMess" : 0,
            "tauxDePerte" : 0
        } for gateway in gateways
    }
    for gateway in gateways:
        nbMessTotal = nbMessageTotal(dfESIROI,dateDebut,dateFin,gateway,tabInfo)
        if(nbMessTotal!=0):
            moyenne=nbMessMoyenParJour(dfESIROI,dateDebut,dateFin,gateway,tabInfo)
            date = dateDernierMessage(dfESIROI,gateway)
            tabTrie = triTauxDePerte(dfESIROI["Meta"],dateDebut,dateFin)
            tauxDePerte = tauxDePerteGlobal(gateway.gatewayName,tabTrie)
            tabInfo[gateway.gatewayName]["messMoy"] = round(moyenne,2)
            tabInfo[gateway.gatewayName]["tauxDePerte"] = tauxDePerte
            tabInfo[gateway.gatewayName]["messRecu"] = nbMessTotal
            tabInfo[gateway.gatewayName]["dateLastMess"] = date
        else:
            del(tabInfo[gateway.gatewayName])
    return(tabInfo)
    
def nbMessMoyenParJour(dfESIROI,dateDebut,dateFin,gateway,result):
    nbJour = (dateFin-dateDebut).days
    tab = []
    somme = 0
    moyenne = 0
    for i in range(nbJour):
        tab.append(0)
    for device in dfESIROI["Meta"]:
        tabData = convertJSONtoOBJ(device)
        for passerelle in tabData["rxInfo"]:
            if(passerelle["name"]==gateway.gatewayName):
                date = convertStringToTimeStamp(passerelle["time"])
                compteur = 0
                if(date>dateDebut and date<dateFin):
                    for i in range(1,len(tab)+1):
                        startDateDay = dateDebut + datetime.timedelta(days=compteur)
                        endDateDay = dateDebut + datetime.timedelta(days=i)
                        if(date>startDateDay and date<endDateDay):
                            tab[compteur] += 1
                        compteur+=1
    for i in range(len(tab)):
        somme += tab[i]
    moyenne = somme / len(tab)
    return moyenne

def nbMessageTotal(dfESIROI,dateDebut,dateFin,gateway,result):
    nbMessTotal = 0
    for device in dfESIROI["Meta"]:
        tabData = convertJSONtoOBJ(device)
        for passerelle in tabData["rxInfo"]:
            if(passerelle["name"]==gateway.gatewayName):
                date = convertStringToTimeStamp(passerelle["time"])
                if(date>dateDebut and date<dateFin):
                    nbMessTotal = nbMessTotal + 1
    return nbMessTotal

def dateDernierMessage(dfESIROI,gateway):
    date = ""
    for device in dfESIROI["Meta"]:
        tabData = convertJSONtoOBJ(device)
        for passerelle in tabData["rxInfo"]:
            if(passerelle["name"]==gateway.gatewayName):
                date = convertStringToTimeStamp(passerelle["time"])
                return date

def nbMessCapteurGateway(dfESIROI,dateDebut,dateFin,gateway,capteur):
    compteur = 0
    for device in dfESIROI["Meta"]:
        tabData = convertJSONtoOBJ(device)
        if(tabData["deviceName"] == capteur):
            for passerelle in tabData["rxInfo"]:
                if(passerelle["name"] == gateway):
                    date = convertStringToTimeStamp(passerelle["time"])
                    if(date>dateDebut and date<dateFin):
                        compteur+=1
    return compteur

def tabNbMessCapteurGateway(dfESIROI,dateDebut,dateFin,gateways,capteurs):
    res = {
        gateway.gatewayName : {
            capteur : 0 for capteur in capteurs
        }
        for gateway in gateways
    }
    for gateway in gateways:
        for capteur in capteurs:
            nbMess = nbMessCapteurGateway(dfESIROI,dateDebut,dateFin,gateway.gatewayName,capteur)
            res[gateway.gatewayName][capteur] = nbMess
    return(res)

def nbMessageParJour(dateDebut,dateFin,dfESIROI):
    compteur = 0
    for device in dfESIROI["Meta"]: #par ligne
        tabData = convertJSONtoOBJ(device)
        for passerelle in tabData["rxInfo"]: #par gateway
            date = convertStringToTimeStamp(passerelle["time"])
            if(date>dateDebut and date<dateFin):
                compteur=compteur+1
    return compteur 

def nbMessageCapteur(dateDebut,dateFin,dfESIROI,capteur):
    compteur = 0
    for device in dfESIROI["Meta"]: #par ligne
        tabData = convertJSONtoOBJ(device)
        if(tabData["deviceName"] == capteur):
            for passerelle in tabData["rxInfo"]: #par gateway
                date = convertStringToTimeStamp(passerelle["time"])
                if(date>dateDebut and date<dateFin):
                    compteur=compteur+1
    return compteur
def nbMessageGateway(dateDebut,dateFin,dfESIROI,gateway):
    compteur = 0
    for device in dfESIROI["Meta"]: #par ligne
        tabData = convertJSONtoOBJ(device)
        for passerelle in tabData["rxInfo"]: #par gateway
            if(passerelle["name"] == gateway):
                date = convertStringToTimeStamp(passerelle["time"])
                if(date>dateDebut and date<dateFin):
                    compteur=compteur+1
    return compteur

def nbMessageParPeriode(dateDebut,dateFin,dfESIROI,gateway,capteur):
    nbJour = (dateFin-dateDebut).days
    tab = {
        "TimeStamp" : [],
        "NombreDeMessage" : []
    }
    compteur = 0
    if(gateway=="tout" and capteur=="tout"):
        print("----------------CAS N°1 ----------------")
        print("Capteur : vide | gateway : vide")
        for jour in range(1,nbJour+1):
            startDateDay = dateDebut + datetime.timedelta(days=compteur)
            endDateDay = dateDebut + datetime.timedelta(days=jour)
            nbMess = nbMessageParJour(startDateDay,endDateDay,dfESIROI)
            print("Date de debut : ",startDateDay)
            print("Date de fin : ",endDateDay)
            print("Nombre de message jour  : ",nbMess)
            print("----------------------------------")
            tab["TimeStamp"].append(startDateDay)
            tab["NombreDeMessage"].append(nbMess)
            compteur+=1
    elif(gateway=="tout" and capteur!=""):
        print("----------------CAS N°2 ----------------")
        print("Capteur : ",capteur," | gateway : vide")
        for jour in range(1,nbJour+1):
            startDateDay = dateDebut + datetime.timedelta(days=compteur)
            endDateDay = dateDebut + datetime.timedelta(days=jour)
            nbMess = nbMessageCapteur(startDateDay,endDateDay,dfESIROI,capteur)
            print("Date de debut : ",startDateDay)
            print("Date de fin : ",endDateDay)
            print("Nombre de message jour  : ",nbMess)
            print("----------------------------------")
            tab["TimeStamp"].append(startDateDay)
            tab["NombreDeMessage"].append(nbMess)
            compteur+=1
    elif (gateway!="" and capteur=="tout"):
        print("----------------CAS N°3 ----------------")
        print("Capteur : vide | gateway : ",gateway)
        for jour in range(1,nbJour+1):
            startDateDay = dateDebut + datetime.timedelta(days=compteur)
            endDateDay = dateDebut + datetime.timedelta(days=jour)
            nbMess = nbMessageGateway(startDateDay,endDateDay,dfESIROI,gateway)
            print("Date de debut : ",startDateDay)
            print("Date de fin : ",endDateDay)
            print("Nombre de message jour  : ",nbMess)
            print("----------------------------------")
            tab["TimeStamp"].append(startDateDay)
            tab["NombreDeMessage"].append(nbMess)
            compteur+=1
    else:
        print("----------------CAS N°4 ----------------")
        print("Capteur : ",capteur," | gateway : ",gateway)
        for jour in range(1,nbJour+1):
            startDateDay = dateDebut + datetime.timedelta(days=compteur)
            endDateDay = dateDebut + datetime.timedelta(days=jour)
            nbMess = nbMessCapteurGateway(dfESIROI,startDateDay,endDateDay,gateway,capteur)
            print("Date de debut : ",startDateDay)
            print("Date de fin : ",endDateDay)
            print("Nombre de message jour  : ",nbMess)
            print("----------------------------------")
            tab["TimeStamp"].append(startDateDay)
            tab["NombreDeMessage"].append(nbMess)
            compteur+=1
    print("Tableau obtenu : ",tab)
    df = pd.DataFrame(tab)
    figure = px.scatter(df,x="TimeStamp",y="NombreDeMessage")
    return(figure)



    
# dateDebut = convertStringToTimeStamp("2021-06-24T00:00:00Z")
# dateFin = convertStringToTimeStamp("2021-06-30T00:00:00Z")
# df = pd.read_csv("../datasets/dataESIROI.csv")
# nbMessageParPeriode(dateDebut,dateFin,df,"","")
# nbMessageParPeriode(dateDebut,dateFin,df,"","IOT-014")
# nbMessageParPeriode(dateDebut,dateFin,df,"EuropaDune","")
# nbMessageParPeriode(dateDebut,dateFin,df,"EuropaDune","IOT-014")
# localisations = getAllLocation(df)
# gateways = getAllGateway(df,localisations)
# devices = getAllDevice(df,gateways)
# # nbMessCapteurGateway(df,dateDebut,dateFin,"EuropaDune","IOT-014")
# tabNbMessCapteurGateway(df,dateDebut,dateFin,gateways,devices)