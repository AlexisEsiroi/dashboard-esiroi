# region Importation des différents packets
import dash
from dash import dcc
import dash_bootstrap_components as dbc
from dash import html
from dash.dependencies import Input, Output, State
from dash import dash_table as dt

import datetime
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
from lib import librairieGeolocalisation as lb

# Connect to main app.py file
from app import app
from app import server

# Connect to your app pages
from apps import network, biologie, map
import pathlib


# endregion

# region Lecture des données CSV dans le dossier spécial
PATH = pathlib.Path(__file__).parent
DATA_PATH = PATH.joinpath("datasets").resolve()
dataFrameESIROI = pd.read_csv(DATA_PATH.joinpath("dataESIROI.csv"))
dataFrameGateway = pd.read_csv(DATA_PATH.joinpath("gateways_info_summary.csv"))
# endregion
localisation = lb.getAllLocation(dataFrameESIROI)
gateways = lb.getAllGateway(dataFrameESIROI, localisation)

app.layout = html.Div([
    dbc.NavbarSimple(
        children=[
            dbc.NavItem(dbc.NavLink("Home", href="/")),
            dbc.NavItem(dbc.NavLink("Network data", href="/apps/network")),
            dbc.NavItem(dbc.NavLink("Biological data", href="/apps/biologie")),
        ],
        brand="Turtles Data Analysis Dashboard",
        color="primary",
        dark=True,
    ),
    dcc.Location(id='url', refresh=False),
    html.Div(id='page-content', children=[]),
])

# region Traitement du changement de page


@app.callback(Output('page-content', 'children'),
              [Input('url', 'pathname')])
def display_page(pathname):
    print(pathname)
    if(pathname == "/"):
        return map.layout
    if pathname == '/apps/network':
        return network.layout
    if pathname == '/apps/biologie':
        return biologie.layout
# endregion

# region Génération automatique de nombre de gateway (camenber)


@app.callback(
    Output("graphNbCapteurGateway", "figure"),
    [Input("genererGraphiqueLocalisationCapteur", "n_clicks")],
    [State("rangeLocalisationCapteur", "start_date"), State(
        "rangeLocalisationCapteur", "end_date"), State("localisationCamenber", "value")],
)
def setNbCapteurs(n_clicks, start_date, end_date, capteur):
    if(capteur == None):
        return dash.no_update
    else:
        tab = lb.nbDeCapteursParGateway(dataFrameESIROI, [capteur], gateways, lb.convertDateStringToTimeStamp(start_date), lb.convertDateStringToTimeStamp(end_date))
        graph = px.pie(tab, values="capteurs", names="gateways")
        return graph
# endregion

# region Mise à jour automatique des gateways


@app.callback(
    Output("ChoixGateway", "options"),
    Input("ChoixLocalisation", "value")
)
def set_gateways(localisation):
    gateways = lb.getAllGateway(dataFrameESIROI, [localisation])
    return [{'label': gateway.gatewayName, 'value': gateway.gatewayName} for gateway in gateways]
# endregion

# region Mise à jour des Capteurs en fonction de la passerelle selectionnée


@app.callback(
    Output("ChoixDevice", "options"),
    Input("ChoixGateway", "value")
)
def set_gateways(device):
    devices = lb.getAllDevice(dataFrameESIROI, [device])
    return [{'label': device, 'value': device} for device in devices]
# endregion

# region Génération du graphique en prenant l'état de tous les valeurs selectionnées
@app.callback(
    Output("graphiqueSNRRSSI", "figure"),
    [Input('generationGraphique', 'n_clicks')],
    [State("ChoixX", "value"), State("ChoixLocalisation", "value"), State("ChoixGateway", "value"), State("ChoixDevice", "value"), State(
        "choixTypeGraphique", "value"), State("my-date-picker-range", "start_date"), State("my-date-picker-range", "end_date")]
)
def generationGraph(n_clicks, x, localisation, gateway, device, typeGraphique, start_date, end_date):
    if(device == None):
        return dash.no_update
    else:
        start_date = lb.convertDateStringToTimeStamp(start_date)
        end_date = lb.convertDateStringToTimeStamp(end_date)
        return lb.genererGraphique(x, localisation, gateway, device, typeGraphique, start_date, end_date, dataFrameESIROI)
# endregion$

# region  Génération du graphique du nombre de message envoyé par jour

@app.callback(
    Output("GraphiqueNombreDeMessage", "figure"),
    [Input('graphiqueMessagesEnvoyes', 'n_clicks')],
    [State("ChoixMessagesGateway", "value"), State("ChoixMessagesDevice", "value"), State("Time", "start_date"), State("Time", "end_date")]
)
def graphiqueNbMess(n_clicks, gateway, device, start_date, end_date):
    start_date = lb.convertDateStringToTimeStamp(start_date)
    end_date = lb.convertDateStringToTimeStamp(end_date)
    return lb.nbMessageParPeriode(start_date,end_date,dataFrameESIROI,gateway,device)
# endregion

# region Mise à jour du tableau automatiquement
@app.callback(
    [Output("table", "data"), Output("table", "columns")],
    [Input("updateTableau", "n_clicks")],
    [State("DateTableau", "start_date"), State("table", "columns"), State(
        "DateTableau", "start_date"), State("DateTableau", "end_date")],
)
def updateTab(n_clicks, data, columns, start_date, end_date):
    print(start_date)
    print(end_date)
    start_date = lb.convertDateStringToTimeStamp(start_date)
    end_date = lb.convertDateStringToTimeStamp(end_date)
    df = pd.DataFrame(lb.genererDataFrameTab(
        dataFrameESIROI, start_date, end_date, gateways))
    data = df.to_dict("records")
    columns = [{"name": i, "id": i} for i in df.columns]
    return data, columns
# endregion


if __name__ == '__main__':
    app.run_server(debug=False)
